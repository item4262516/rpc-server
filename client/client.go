package main

import (
	"fmt"
	"log"
	"net/rpc"

	"gitlab.com/item4262516/rpc-server/config"
)

// go run client/client.go
// 2024/04/19 03:22:22 CalcAdd result: 8
func main() {
	config := config.New()
	rpcClient, err := rpc.Dial(config.Scheme, fmt.Sprintf("%s:%d", config.Host, config.Port))
	if err != nil {
		log.Fatal("Failed to dial server:", err)
	}
	defer rpcClient.Close()

	var reply int64
	err = rpcClient.Call("Service_Calculator.CalcAdd", [2]int64{5, 3}, &reply)
	if err != nil {
		log.Fatal("Failed to call CalcAdd:", err)
	} else {
		log.Println("CalcAdd result:", reply)
	}
}
