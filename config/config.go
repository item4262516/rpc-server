package config

type Config struct {
	Scheme string
	Host   string
	Port   int
}

func New() *Config {
	return &Config{
		Scheme: "tcp",
		Host:   "127.0.0.1",
		Port:   8089,
	}
}
