package main

import (
	"fmt"
	"log"
	"net"
	"net/rpc"

	"gitlab.com/item4262516/rpc-server/config"
	"gitlab.com/item4262516/rpc-server/server"
)

func main() {
	// 读取配置
	config := config.New()
	// 服务列表
	servers := []any{new(server.Service_Calculator)}
	// 遍历服务列表，注册服务
	for i, server := range servers {
		server := server // 兼容旧版本
		err := rpc.Register(server)
		if err != nil {
			log.Fatal(i, "=>Failed to register Service_Calculator", err)
		}
	}
	log.Println("Service_Calculator registered")
	listener, err := net.Listen(config.Scheme, fmt.Sprintf("%s:%d", config.Host, config.Port))
	if err != nil {
		log.Fatal("Failed to listen:", err)
	}
	for {
		conn, err := listener.Accept()
		if err != nil {
			log.Println("Failed to dial server:", err)
		}
		go rpc.ServeConn(conn)
	}
}
