package server

import (
	"fmt"
	"log"
	"net/rpc"
	"testing"

	"gitlab.com/item4262516/rpc-server/config"
)

func callMethod(method string, args interface{}) {
	config := config.New()
	rpcClient, err := rpc.Dial(config.Scheme, fmt.Sprintf("%s:%d", config.Host, config.Port))
	if err != nil {
		log.Fatal("Failed to dial server:", err)
	}
	defer rpcClient.Close()
	var reply int64
	err = rpcClient.Call(method, args, &reply) // 测试在这里统一处理错误
	if err != nil {
		log.Fatal("Failed to call "+method+":", err)
	} else {
		log.Println(method+" result:", reply)
	}
}

func TestCalculator(t *testing.T) {
	t.Run("加法", func(t *testing.T) {
		callMethod("Service_Calculator.CalcAdd", [2]int64{10, 5})
	})

	t.Run("减法", func(t *testing.T) {
		callMethod("Service_Calculator.CalcSub", [2]int64{10, 5})
	})

	t.Run("乘法", func(t *testing.T) {
		callMethod("Service_Calculator.CalcMul", [2]int64{10, 5})
	})

	t.Run("除法", func(t *testing.T) {
		callMethod("Service_Calculator.CalcDiv", [2]int64{10, 5})
	})

	t.Run("除法(除数=0)", func(t *testing.T) {
		callMethod("Service_Calculator.CalcDiv", [2]int64{10, 0})
	})
}
