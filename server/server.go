package server

import (
	"errors"
)

type Service_Calculator int

func (sc *Service_Calculator) CalcAdd(arg [2]int64, reply *int64) error {
	*reply = arg[0] + arg[1]
	return nil
}

func (sc *Service_Calculator) CalcSub(arg [2]int64, reply *int64) error {
	*reply = arg[0] - arg[1]
	return nil
}

func (sc *Service_Calculator) CalcMul(arg [2]int64, reply *int64) error {
	*reply = arg[0] * arg[1]
	return nil
}

func (sc *Service_Calculator) CalcDiv(arg [2]int64, reply *int64) error {
	if arg[1] == 0 {
		return errors.New("divide by zero")
	}
	*reply = arg[0] / arg[1]
	return nil
}
